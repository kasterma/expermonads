(ns expermonads.core
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [clojure.tools.trace :as tt]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo]
            [clojure.core.logic :as logic]
            [clojure.algo.monads :as monads])
  (:use midje.sweet)
  (:gen-class))

(matrix/set-current-implementation :vectorz)

(defn tt []
  (monads/domonad monads/sequence-m
             [a (range 6 8)
              b ((fn [x] (println x) (range 5 x)) a)]
             (+ a b)))

(monads/with-monad monads/sequence-m (m-bind (range 0 3) (fn [x] (m-result (* x x)))))

(comment
  (monads/domonad monads/maybe-m
             [a 3
              b ((fn [x] (if (> (rand) 0.5) 4 nil)) a)
              c ((fn [x] (* x x)) a)]
             [a b c])
)
